/**
* Copyright(c) 2014, Andrew J. McPherson.
* All rights reserved.
* 
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
* 
* 1. Redistributions of source code must retain the above copyright notice, this
* list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright notice,
* this list of conditions and the following disclaimer in the documentation
* and/or other materials provided with the distribution.
* 
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
* ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
* ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
* */
#include <iostream>
#include <map>
#include <set>
#include <fstream>
#include <assert.h>
#include "pin.H"
#include <string>
#include <vector>
#include <stdlib.h>


static KNOB<BOOL>   KnobSameThread(KNOB_MODE_WRITEONCE, "pintool", "same", "0", "consider release acquire relationships in the same thread.");
static KNOB<UINT32> KnobDistance(KNOB_MODE_WRITEONCE, "pintool", "distance", "5", "Allowable distance between a read and condition for an acquire.");

PIN_LOCK lock_lastWriter;
PIN_LOCK lock_nthreads;
INT32 numThreads = 0;

typedef struct
{
    THREADID tid;
    ADDRINT w;
    INT32 line;
    string file;
} writer;


typedef struct
{
    ADDRINT r;
    INT32 line;
    string file;
    UINT64 instNum;
//    UINT64 firstFound;
    UINT64 minDistance;
    THREADID tid;
    
    ADDRINT w;
    INT32 wLine; //if -1 then cold read
    string wFile;
    THREADID wTid;
} reader;


class thread_data
{
  public:
        thread_data(): instCount(0){}

    std::map<UINT32, std::set<reader*> > regFile; 
    std::set<reader*> insBuffer;
    std::set<reader*> potentialAcquires;
    std::map<std::string, std::set<UINT32> > reads;
    std::map<std::string, std::set<UINT32> > writes;
    UINT64 instCount;
};


// global store of last writer information for each address
std::map<ADDRINT, writer> lastWriter;

// global store of memory status
std::map<ADDRINT, INT32> isShared;

//TLS Key
static TLS_KEY key;

// Get Thread Local Storage
thread_data* get_tls (THREADID tid)
{
    return static_cast<thread_data*>(PIN_GetThreadData(key, tid));
}


// Thread startup
VOID ThreadStart (THREADID threadid, CONTEXT *ctxt, INT32 flags, VOID *v)
{
    PIN_GetLock(&lock_nthreads, threadid+1);
    numThreads++;
    PIN_ReleaseLock(&lock_nthreads);
    thread_data* t = new thread_data;
    PIN_SetThreadData(key, t, threadid);
}


// Actions taken on a load
VOID onLoad (ADDRINT ip, ADDRINT addr, THREADID tid, UINT32 ins)
{
    INT32 line = 0;
    INT32 column = 0;
    string file = "";

    PIN_LockClient();
    PIN_GetSourceLocation (ip, &column, &line, &file);
    PIN_UnlockClient();

    //If read is not from a known source file, ignore
    if (line == 0)
        return;

    thread_data* tdata = get_tls(tid);

    // Create reader object in heap
    reader *read = new reader();
    read->r = addr;
    read->line = line;
    read->file = file;
    read->instNum = tdata->instCount;
   // read->firstFound = 0;
    read->minDistance = 0;
    read->tid= tid;

    tdata->reads[file].insert(line);

    PIN_GetLock (&lock_lastWriter, tid+1);

    
 //   std::cerr << "R:T" << tid << ":" << file.substr(file.rfind("/")+1) << ":" << line << " " << read << " COL:" << column << std::endl;
    

    // Update isShared information
    if (isShared.count (addr) && isShared[addr] != (INT32) tid)
        isShared[addr] = -1;
    else
        isShared[addr] = tid;

    if (lastWriter.count (addr))
    {
        read->w = lastWriter[addr].w;
        read->wLine = lastWriter[addr].line;
        read->wFile = lastWriter[addr].file;
        read->wTid = lastWriter[addr].tid;
    }
    else
        read->wLine = -1;

    PIN_ReleaseLock (&lock_lastWriter);

    // Put pointer in insBuffer
    tdata->insBuffer.insert(read);
}


// Actions taken on a store
VOID onStore (ADDRINT ip, ADDRINT addr, THREADID tid)
{
    INT32 line = 0;
    INT32 column = 0;
    string file="";

    PIN_LockClient();
    PIN_GetSourceLocation (ip, &column, &line, &file);
    PIN_UnlockClient();

    //If write is not from a known source file, ignore
    //FIXME  this may misallocate new reads to an older write, perhaps I should clear store
    if (line == 0)
        return;

    thread_data* tdata = get_tls(tid);

    writer newWriter;
    newWriter.tid = tid;
    newWriter.w = addr;
    newWriter.line = line;
    newWriter.file = file;

    tdata->writes[file].insert(line);

    PIN_GetLock (&lock_lastWriter, tid+1);

//    std::cerr << "W:T" << tid << ":" << file.substr(file.rfind("/")+1) << ":" << line << endl;

    // Update isShared information
    if (isShared.count (addr) && isShared[addr] != (INT32) tid)
        isShared[addr] = -1;
    else
        isShared[addr] = tid;

    // Create new entry in table
    lastWriter[addr] = newWriter;

    PIN_ReleaseLock (&lock_lastWriter);
}


// Add taint info to insBuffer
VOID onRegRead (UINT32 r, THREADID tid)
{
    thread_data* tdata = get_tls(tid);
    tdata->insBuffer.insert (tdata->regFile[r].begin (), tdata->regFile[r].end ());
}

// If branchDecision add to potentialAcquires
VOID branchDecision(THREADID tid)
{
    	thread_data* tdata = get_tls(tid);
	for (std::set<reader*>::iterator it = tdata->insBuffer.begin (); 
                     it != tdata->insBuffer.end (); ++it)
        {
            if ((*it)->minDistance == 0)
                (*it)->minDistance = tdata->instCount - (*it)->instNum;

            	tdata->potentialAcquires.insert (*it);
        }
}


VOID onRegWrite (UINT32 w, THREADID tid)
{
    thread_data* tdata = get_tls (tid);
    tdata->regFile[w] = tdata->insBuffer;
}

VOID clearBuffer (THREADID tid)
{
    thread_data* tdata = get_tls (tid);
    tdata->insBuffer.clear ();
}


VOID twoRead(THREADID tid)
{
    std::cerr << "2ND READ" << std::endl;
}


VOID instCounter(THREADID tid)
{
    thread_data* tdata = get_tls (tid);
    tdata->instCount++;
}


VOID Instruction (INS ins, VOID *v)
{
      INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)instCounter, IARG_THREAD_ID, IARG_END);

      if (INS_IsMemoryRead (ins))
    {
        UINT32 I = INS_Opcode(ins);

        INS_InsertPredicatedCall (
            ins, IPOINT_BEFORE, (AFUNPTR)onLoad,
            IARG_INST_PTR,
            IARG_MEMORYREAD_EA,
            IARG_THREAD_ID,
            IARG_UINT32, I,
            IARG_END);
    } 
    if (INS_HasMemoryRead2 (ins))
            INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)twoRead, IARG_THREAD_ID, IARG_END);

    if (INS_IsMemoryWrite (ins))
    {
        INS_InsertPredicatedCall (
            ins, IPOINT_BEFORE, (AFUNPTR)onStore,
            IARG_INST_PTR,
            IARG_MEMORYWRITE_EA,
            IARG_THREAD_ID,
            IARG_END);
    }


    bool isBranchDecision = INS_Valid (INS_Next (ins)) && 
        INS_Category (INS_Next(ins)) == XED_CATEGORY_COND_BR;

    INT32 maxR = INS_MaxNumRRegs (ins);
    for (int i = 0; i < maxR; ++i)
    {
        const REG reg = INS_RegR (ins, i);

        if (REG_valid (reg))
        {
            INS_InsertCall (ins, IPOINT_BEFORE, 
                    (AFUNPTR)onRegRead, 
                    IARG_UINT32, reg,
                    IARG_THREAD_ID,
                    IARG_END);
        }
    }

    if (isBranchDecision)
    	INS_InsertCall (ins, IPOINT_BEFORE, (AFUNPTR)branchDecision, IARG_THREAD_ID, IARG_END);

    bool untaint = (INS_Mnemonic (ins) == "XOR" && (UINT32) INS_RegR (ins, 0) == (UINT32) INS_RegR (ins, 1)) ? true : false;
        
        
    if (!untaint)
    {

      INT32 maxW = INS_MaxNumWRegs (ins);
      for (int i = 0; i < maxW; ++i)
      {
          const REG reg = INS_RegW (ins, i);
  
          if (REG_valid (reg))
          {
              INS_InsertCall (ins, IPOINT_BEFORE, 
                      (AFUNPTR)onRegWrite, 
                      IARG_UINT32, reg,
                      IARG_THREAD_ID,
                      IARG_END);
          }
      }
    }
    INS_InsertCall (ins, IPOINT_BEFORE, (AFUNPTR)clearBuffer, IARG_THREAD_ID, IARG_END);
}


VOID Fini (INT32 code, VOID *v)
{
    //std::cout << "Fini" << std::endl;
    std::map<std::string, std::set<UINT32> > reads;
    std::map<std::string, std::set<UINT32> > writes;

    for (INT32 i = 0; i < numThreads; i++)
    {
        thread_data* tdata = get_tls(i);
        ///std::cout << "T" << i << " pAcquires:" <<  tdata->potentialAcquires.size () << std::endl;

        for (std::map<std::string, std::set<UINT32> >::iterator it = tdata->reads.begin();
          it != tdata->reads.end (); ++it)
            reads[it->first].insert(it->second.begin (), it->second.end());         

        for (std::map<std::string, std::set<UINT32> >::iterator it = tdata->writes.begin();
          it != tdata->writes.end (); ++it)
            writes[it->first].insert(it->second.begin (), it->second.end());         

        for (std::set<reader*>::iterator it = tdata->potentialAcquires.begin ();
            it != tdata->potentialAcquires.end (); ++it)
        {
        //    debugRegisterPrintout (i);
            if (isShared[(*it)->r] == -1)// && (*it)->minDistance < KnobDistance)
                std::cerr << "Acquire:" << (*it)->file.substr((*it)->file.rfind("/")+1) << ":" << (*it)->line << ":" << 
                "Release:" << (*it)->wFile.substr((*it)->wFile.rfind("/")+1) << ":" << (*it)->wLine << ":" << (*it)->minDistance <<std::endl;
        }
    }

    UINT32 readCount = 0;
    UINT32 writeCount = 0;
    for (std::map<std::string, std::set<UINT32> >::iterator it = reads.begin();
      it != reads.end (); ++it)
      readCount += it->second.size();
    for (std::map<std::string, std::set<UINT32> >::iterator it = writes.begin();
      it != writes.end (); ++it)
      writeCount += it->second.size();
    
    std::cerr << "Total Reads: " << readCount << std::endl; 
    std::cerr << "Total Writes: " << writeCount << std::endl; 
}


INT32 Usage ()
{
    cerr << "TODO" << endl;
    return -1;
}


int main (int argc, char ** argv)
{
      PIN_InitSymbols ();
    if (PIN_Init (argc, argv))
        return Usage ();

    PIN_InitLock (&lock_lastWriter);
    PIN_InitLock (&lock_nthreads);

        PIN_AddThreadStartFunction(ThreadStart, 0);
    INS_AddInstrumentFunction(Instruction, 0);
    PIN_AddFiniFunction (Fini, 0);
    PIN_StartProgram();

    return 0;
}

