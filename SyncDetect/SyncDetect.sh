#!/bin/bash

loc=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

if [[ `uname` == 'Darwin' ]]; then
	$loc/pin/pin -t $loc/obj-intel64/SyncDetect2.dylib -- $@
else
	$loc/pin/pin.sh -t $loc/obj-intel64/SyncDetect2.so -- $@
fi
