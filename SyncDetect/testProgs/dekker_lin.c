//Dekker algorithm -- Changhui Lin
#include<stdlib.h>
#include<time.h>
#include<unistd.h>
#include<pthread.h>
#include<stdio.h>
#define NLOOP 3
#define ARRAYSIZE 10
#define STEP (ARRAYSIZE/2)

#define DEBUG
#define WORK

int flag[2]={0};
int turn=0;

double **array0;
double **array1;

void working()
{
	int i;
	float a, b;
	for(i=0;i<1000;i++)
	{
		a=1.0*i*(i+1);
	}
}


void* p0(void *args)
{
	int i, loop;

	for(loop=0; loop<NLOOP; loop++)
	{
		int j, k;
		for(j=0; j<ARRAYSIZE; j=j+STEP)
		{
			double read=array0[j][j];
			k = (j+ARRAYSIZE/2);
			if(k>=ARRAYSIZE)
				k=k-ARRAYSIZE;
			array0[k][k]=j;
		}	

		flag[0]=1;
		sleep (2);
		while(flag[1])
		{
			sleep (2);
			if(turn)
			{
				sleep (2);
				flag[0]=0;
				while(turn);
				flag[0]=1;
			}
		}

		//Critical Section
#ifdef DEBUG
		printf("In p0[0]\n");
#endif
#ifdef WORK
		working();
#endif
		turn=1;
		flag[0]=0;
	}
	return NULL;
}


void *p1(void *arg)
{
	int i, loop;

	for(loop=0; loop<NLOOP; loop++)
	{
		int j,k;
		for(j=0; j<ARRAYSIZE; j=j+STEP)
		{
			double read=array1[j][j];
			k = (j+ARRAYSIZE/2);
			if(k>=ARRAYSIZE)
				k=k-ARRAYSIZE;
			array1[k][k]=j;
		}	
		flag[1]=1;

		sleep (1);
		while(flag[0])
		{
			sleep (1);
			if(!turn)
			{
				sleep (1);
				flag[1]=0;
				while(!turn);
				flag[1]=1;
			}
		}

		//Critical Section
#ifdef DEBUG
		printf("\t\t\t\tIn p1[1]\n");
#endif
#ifdef WORK
		working();
#endif
		turn=0;
		flag[1]=0;
	}
	return NULL;

}


int main()
{
	int i=0,j=0, ret=0;
	pthread_t id0, id1;
	clock_t start, end;
	double total;

	array0=(double **)malloc(ARRAYSIZE*sizeof(double *));
	array1=(double **)malloc(ARRAYSIZE*sizeof(double *));

	for(i=0;i<ARRAYSIZE;i++)
	{
		array0[i]=(double *)malloc(ARRAYSIZE*sizeof(double));	
		if(array0[i]==NULL)
			printf("ERROR3\n");
		array1[i]=(double *)malloc(ARRAYSIZE*sizeof(double));	
		if(array1[i]==NULL)
			printf("ERROR4\n");
	}

	for(i=0;i++;i<ARRAYSIZE)
		for(j=0;j++;j<ARRAYSIZE)
		{
			array0[i][j]=0;
			array1[i][j]=0;
		}

	ret = pthread_create(&id0, NULL, &p0, NULL);
/*
	if (ret)
	{
		printf("Create pthread error!\n");
		return 1;
	}
*/

	ret = pthread_create(&id1, NULL, &p1, NULL);
/*
	if (ret)
	{
		printf("Create pthread error!\n");
		return 1;
	}
*/

	pthread_join(id0, NULL);
	pthread_join(id1, NULL);
	return 0;
}
