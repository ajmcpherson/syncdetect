/**
* Copyright(c) 2014, Andrew J. McPherson.
* All rights reserved.
* 
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
* 
* 1. Redistributions of source code must retain the above copyright notice, this
* list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright notice,
* this list of conditions and the following disclaimer in the documentation
* and/or other materials provided with the distribution.
* 
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
* ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
* ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
* */
#include <iostream>
#include <map>
#include <set>
#include <fstream>
#include <assert.h>
#include "pin.H"
#include <string>
#include <vector>
#include <stdlib.h>


static KNOB<BOOL>   KnobSameThread(KNOB_MODE_WRITEONCE, "pintool", "same", "0", "consider release acquire relationships in the same thread.");
static KNOB<UINT32> KnobDistance(KNOB_MODE_WRITEONCE, "pintool", "distance", "5", "Allowable distance between a read and condition for an acquire.");

PIN_LOCK lock_lastWriter;
PIN_LOCK lock_nthreads;
INT32 numThreads = 0;

typedef struct
{
	ADDRINT r;
	INT32 line;
	string file;
	UINT64 instNum;
	UINT64 firstFound;
} reader;

typedef struct
{
	THREADID tid;
	ADDRINT w;
	INT32 line;
	string file;
	std::set<reader*> readers;
} writer;

class thread_data
{
  public:
    	thread_data(): instCount(0){}

	std::map<UINT32, std::set<reader*> > regFile; 
	std::set<reader*> insBuffer;
	std::set<reader*> potentialAcquires;
	UINT64 instCount;
};

// global store of last writer information for each address
std::map<ADDRINT, writer> lastWriter;

// Permanent store of writes to sets of reads
std::vector<writer> lastWriterArchive;

//TLS Key
static TLS_KEY key;

// Get Thread Local Storage
thread_data* get_tls (THREADID tid)
{
	return static_cast<thread_data*>(PIN_GetThreadData(key, tid));
}


// Thread startup
VOID ThreadStart (THREADID threadid, CONTEXT *ctxt, INT32 flags, VOID *v)
{
    PIN_GetLock(&lock_nthreads, threadid+1);
    numThreads++;
    PIN_ReleaseLock(&lock_nthreads);
    thread_data* t = new thread_data;
    PIN_SetThreadData(key, t, threadid);
}

VOID debugRegisterPrintout (THREADID tid)
{
	thread_data* tdata = get_tls (tid);

	for (std::map<UINT32, std::set<reader*> >::iterator it = 
		tdata->regFile.begin (); it != tdata->regFile.end ();
		++it)
	{
		std::cout << it->first << " : ";

		for (std::set<reader*>::iterator jt = it->second.begin();
			jt != it->second.end (); ++jt)
			std::cout << *jt << " ";

		std::cout << std::endl;
	}
	std::cout << std::endl;
}

VOID debugLastWriter (THREADID tid)
{
	PIN_GetLock (&lock_lastWriter, tid+1);
	// printout lastWriter
	std::cout << "LastWriter" << std::endl;

	for (std::map<ADDRINT, writer>::iterator it = lastWriter.begin ();
		it != lastWriter.end (); ++it)
	{
		std::cout << it->first << " : ";

		for (std::set<reader*>::iterator jt = it->second.readers.begin ();
			jt != it->second.readers.end (); ++jt)
		{
			std::cout << *jt << ", ";
		}
		std::cout << std::endl;
	}
	std::cout << std::endl << std::endl;

	// printout lastWriterArchive
	std::cout << "LastWriterArchive" << std::endl;

	for (std::vector<writer>::iterator it = lastWriterArchive.begin ();
		it != lastWriterArchive.end (); ++it)
	{
		std::cout << it->w << " : ";

		for (std::set<reader*>::iterator jt = it->readers.begin ();
			jt != it->readers.end (); ++jt)
		{
			std::cout << *jt << ", ";
		}
		std::cout << std::endl;
	}
	std::cout << std::endl << std::endl;

	PIN_ReleaseLock (&lock_lastWriter);
}

// Actions taken on a load
VOID onLoad (ADDRINT ip, ADDRINT addr, THREADID tid, UINT32 ins)
{
	INT32 line = 0;
	INT32 column = 0;
	string file = "";

	PIN_LockClient();
	PIN_GetSourceLocation (ip, &column, &line, &file);
	PIN_UnlockClient();

	//If read is not from a known source file, ignore
	if (line == 0)
		return;

	thread_data* tdata = get_tls(tid);

	// Create reader object in heap
	reader *read = new reader();//(reader*) malloc (sizeof (reader));
	read->r = ip;
	read->line = line;
	read->file = file;
	read->instNum = tdata->instCount;
	read->firstFound = 0;

	PIN_GetLock (&lock_lastWriter, tid+1);
	std::cout << "READ:T" << tid << ":" << file << ":" << line << ":" << " " << read << " " << addr << " " << OPCODE_StringShort(ins) << endl;


	// Test if address is in last Writer table if not free reader 
	if (!lastWriter.count (addr) || (!KnobSameThread && lastWriter[addr].tid == tid))
	{
	//	std::cout << "QUITTING" << std::endl;
		free (read);
		PIN_ReleaseLock (&lock_lastWriter);
		return;
	}

	// Put pointer in lastWriterTable
	lastWriter[addr].readers.insert(read);

	PIN_ReleaseLock (&lock_lastWriter);

	// Put pointer in insBuffer
	tdata->insBuffer.insert(read);

	//debugRegisterPrintout (tid);

	//debugLastWriter (tid);
}

VOID onStore (ADDRINT ip, ADDRINT addr, THREADID tid)
{
	INT32 line = 0;
	INT32 column = 0;
	string file="";

	PIN_LockClient();
	PIN_GetSourceLocation (ip, &column, &line, &file);
	PIN_UnlockClient();

	//If write is not from a known source file, ignore
	//FIXME  this may misallocate new reads to an older write, perhaps I should clear store
	if (line == 0)
		return;


  	writer newWriter;
	newWriter.tid = tid;
	newWriter.w = ip;
	newWriter.line = line;
	newWriter.file = file;

	PIN_GetLock (&lock_lastWriter, tid+1);
	std::cout << "WRITE:T" << tid << ":" << file << ":" << line << " " << addr << endl;

	// If entry exists in lastWriter, move contents to archive
	if (lastWriter.count (addr) && !lastWriter[addr].readers.empty ())
		lastWriterArchive.push_back (lastWriter[addr]);

	// Create new entry in table
	lastWriter[addr] = newWriter;

	PIN_ReleaseLock (&lock_lastWriter);
}

VOID onRegRead (UINT32 r, bool branchDecision, ADDRINT ip, THREADID tid)
{
	thread_data* tdata = get_tls(tid);

	// Add taint info to insBuffer
	tdata->insBuffer.insert (tdata->regFile[r].begin (), tdata->regFile[r].end ());

	// If branchDecision add to potentialAcquires
	if (branchDecision)
	{
	
/*
		std::cout << "BRANCH DECISION reading:" << r << " " << ip << std::endl;

		if (!tdata->regFile[r].empty())
		{
			std::cout << "BRANCH DECISION:";
			for (std::set<reader*>::iterator it = tdata->regFile[r].begin ();
				it != tdata->regFile[r].end (); ++it)
			{
				std::cout << *it << ", ";
			}
			std::cout << std::endl;
		}

*/

		for (std::set<reader*>::iterator it = tdata->insBuffer.begin (); it != tdata->insBuffer.end (); ++it)
		{
			if ((*it)->firstFound == 0)
				(*it)->firstFound = tdata->instCount;
	//		std::cout << "INSERTING:" << *it << std::endl;
			tdata->potentialAcquires.insert (*it);
		}
/*
		tdata->potentialAcquires.insert (tdata->insBuffer.begin (), 
						 tdata->insBuffer.end ());
*/
	}
/*
	std::cout << "RegRead:" << r << ": ";
	for (std::set<reader*>::iterator it = tdata->insBuffer.begin (); it != tdata->insBuffer.end (); ++it)
		std::cout << *it << " ";
	std::cout << std::endl;
*/
}

VOID onRegWrite (UINT32 w, bool final, bool untaint, ADDRINT ip, THREADID tid)
{
	thread_data* tdata = get_tls (tid);

	if (untaint)
		tdata->insBuffer.clear ();


/*
	// Store insBuffer data into taint map
	if (!tdata->insBuffer.empty ())
	{
		std::cout << "RegWrite:" << w << ": ";
		for (std::set<reader*>::iterator it = tdata->insBuffer.begin (); it != tdata->insBuffer.end (); ++it)
			std::cout << *it << " ";
		std::cout << ": " << ip << " isFinal:" << std::dec << final << std::endl;
	}
	else 
	{
		std::cout << "RegWrite:" << w << " empty buffer " << ip << std::endl;
	}

*/

	tdata->regFile[w] = tdata->insBuffer;

	// If final, clear insBuffer
	if (final)
		tdata->insBuffer.clear ();
}

VOID twoRead(THREADID tid)
{
	std::cout << "2ND READ" << std::endl;
}

VOID instCounter(THREADID tid)
{
	thread_data* tdata = get_tls (tid);
	tdata->instCount++;
        if (tdata->instCount % 10000 == 0)
          std::cerr << "T" << tid << " " tdata->instCount << std::endl;
}

VOID Instruction (INS ins, VOID *v)
{
    	INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)instCounter, IARG_THREAD_ID, IARG_END);

  	if (INS_IsMemoryRead (ins))
	{
		UINT32 I = INS_Opcode(ins);

		INS_InsertPredicatedCall (
		    ins, IPOINT_BEFORE, (AFUNPTR)onLoad,
		    IARG_INST_PTR,
		    IARG_MEMORYREAD_EA,
		    IARG_THREAD_ID,
		    IARG_UINT32, I,
		    IARG_END);
	} 
	if (INS_HasMemoryRead2 (ins))
    		INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)twoRead, IARG_THREAD_ID, IARG_END);

	if (INS_IsMemoryWrite (ins))
	{
		INS_InsertPredicatedCall (
		    ins, IPOINT_BEFORE, (AFUNPTR)onStore,
		    IARG_INST_PTR,
		    IARG_MEMORYWRITE_EA,
		    IARG_THREAD_ID,
		    IARG_END);
	}


	bool isBranchDecision = INS_Valid (INS_Next (ins)) && 
	    INS_Category (INS_Next(ins)) == XED_CATEGORY_COND_BR;

	INT32 maxR = INS_MaxNumRRegs (ins);
	for (int i = 0; i < maxR; ++i)
	{
		const REG reg = INS_RegR (ins, i);

		if (REG_valid (reg))
		{
			INS_InsertCall (ins, IPOINT_BEFORE, 
					(AFUNPTR)onRegRead, 
					IARG_UINT32, reg,
					IARG_BOOL, isBranchDecision,
					IARG_INST_PTR,
					IARG_THREAD_ID,
					IARG_END);
		}
	}

	bool untaint = (INS_Mnemonic (ins) == "XOR" && (UINT32) INS_RegR (ins, 0) == (UINT32) INS_RegR (ins, 1)) ? true : false;
		
		

	INT32 maxW = INS_MaxNumWRegs (ins);
	for (int i = 0; i < maxW; ++i)
	{
		const REG reg = INS_RegW (ins, i);
		bool finalW = i == maxW - 1;

		if (REG_valid (reg))
		{
			INS_InsertCall (ins, IPOINT_BEFORE, 
					(AFUNPTR)onRegWrite, 
					IARG_UINT32, reg,
					IARG_BOOL, finalW,
					IARG_BOOL, untaint,
					IARG_INST_PTR,
					IARG_THREAD_ID,
					IARG_END);
		}
	}
}

typedef struct
{
	string file;
	INT32 line;
	std::set<std::pair<string, INT32> > acquires;
} release;

VOID Fini (INT32 code, VOID *v)
{
	cout << "Fini" << endl;
	// Move all lastWriter entries to archive
	for (std::map<ADDRINT, writer>::iterator it = lastWriter.begin ();
	     it != lastWriter.end (); ++it)
	{
		lastWriterArchive.push_back (it->second);
	}

	//TODO For each TLS go through potential acquires grabbing lines from 
	// lastWriterArchive into final set of write to read sets that should 
	// be reported.  (Use two sets per write for those that meet sig and 
	// those that don't)

	//std::map <writer, std::set<std::pair<const char*, INT32> > > outputTable;
	std::vector<release> releases;

  	for (INT32 i = 0; i < numThreads; i++)
	{
		thread_data* tdata = get_tls(i);
		for (std::set<reader*>::iterator it = tdata->potentialAcquires.begin ();
			it != tdata->potentialAcquires.end (); ++it)
		{
		//	debugRegisterPrintout (i);
			std::cout << "potential Acquire:" << *it << " " << (*it)->file << ":" << (*it)->line << std::endl;

			//find reader pointer in lastWriterArchive
			for (std::vector<writer>::iterator jt = lastWriterArchive.begin ();
				jt != lastWriterArchive.end (); ++jt)
			{
				if (jt->readers.count (*it))
				{
					std::cout << "distance to cond:" << (*it)->firstFound - (*it)->instNum << std::endl;

					//if this read is too far from the cond, ignore it
					if ((*it)->firstFound - (*it)->instNum > KnobDistance)
						continue;

					bool found = false;
					//if writer in outputTable, add to set
					for (std::vector<release>::iterator kt = releases.begin (); kt != releases.end (); ++kt)
					{
						if (jt->line == kt->line && jt->file == kt->file)
						{
							kt->acquires.insert (std::make_pair ((*it)->file, (*it)->line));
							found = true;
							break;
						}
					}
					
					//else add the writer;
					if (!found)
					{
						release newW;
						newW.line = jt->line;
						newW.file = jt->file;
						newW.acquires.insert (make_pair ((*it)->file, (*it)->line));
						releases.push_back(newW);
					}
					break;
				}
				
			}

		}
	}


	for (std::vector<release>::iterator it = releases.begin ();
		it != releases.end (); ++it)
	{
		std::cout << "Release:" << it->file << ":" << it->line << std::endl;

		for (std::set<std::pair<string, INT32> >::iterator jt = it->acquires.begin ();
			jt != it->acquires.end (); ++jt)
		{
			std::cout << "\tAcquire:" << jt->first << ":" << jt->second << std::endl;
		}
	}
}



INT32 Usage ()
{
	cerr << "TODO" << endl;
	return -1;
}

int main (int argc, char ** argv)
{
  	PIN_InitSymbols ();
	if (PIN_Init (argc, argv))
		return Usage ();

	PIN_InitLock (&lock_lastWriter);
	PIN_InitLock (&lock_nthreads);

    	PIN_AddThreadStartFunction(ThreadStart, 0);
	INS_AddInstrumentFunction(Instruction, 0);
	PIN_AddFiniFunction (Fini, 0);
	PIN_StartProgram();

	return 0;
}

